package model.domain;
/**
 * 
 * @author : r0588667 Jeroen Debra
 *
 */

import javax.swing.JOptionPane;

public class Player {

	private String name;
	private int score;
	private Grid grid;
	
	public Player(String name){
		this.setName(name);
		this.grid = new Grid();
		score = 0;
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Grid getGrid() {
		return grid;
	}

	public void ScoreReset(){
	this.score=0;
	}
	
	public void increase(){
		this.score++;
	}
	
	public void decrease(){
		this.score--;
	}
}
