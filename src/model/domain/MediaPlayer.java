package model.domain;

import java.io.File;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

public class MediaPlayer implements LineListener {

	
	public static void playMusic(){
		try{
		    InputStream in;
		    //	in = new FileInputStream(new File("Pirates Of The Caribbean Theme Song.mp3"));
		    //	AudioStream audio  = new AudioStream(in);
		    //	AudioPlayer.player.start(audio);
		    AudioInputStream result =  AudioSystem.getAudioInputStream(new File("Pirates_Of_The_Caribbean_Theme_Song.wav"));
		    Clip clip = AudioSystem.getClip();
		    clip.open(result);
		    clip.start();
		    
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
}
	@Override
	public void update(LineEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
