package model.domain;
/**
 * 
 * @author : r0588667 Lennert Wieers,Jeroen Debra, Robbe Vaes
 *
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;

import model.behaviour.BehaviourFactory;
import model.behaviour.ComputerBehaviour;
import model.behaviour.Place;
import model.placement.ComputerPlacement;
import model.placement.PlacementFactory;
import model.states.GameState;
import model.states.New;
import model.states.Started;

public class Game extends Observable {

	private GameState gameState;
	private Player player1;
	private Player player2;
	private boolean state;
	private ArrayList<SchipType> schips;
	private int x,y;
	private int computerDestroyed;
	private int playerDestroyed;
	private ComputerBehaviour computer;
	private int numberOfSchips;
	private ArrayList<String> errors;
	private SchipType schipType;
	private ComputerPlacement cp;
	private PlacementFactory pf;
	
	
	public Game(String spelernaam){
		computer = BehaviourFactory.getBehaviour();
		setComputerDestroyed(0);
		setGameState(new New());
		setNumberOfSchips(0);
		errors = new ArrayList<String>();
		schips = new ArrayList<SchipType>(Arrays.asList(SchipType.values()));
		pf= new PlacementFactory();
		cp = pf.createPlacementStrategy();
		player2 = new Player("computer");
		player1 = new Player(spelernaam);
		
	}
	public void setState(boolean state) {
		this.state = state;
		noterize();
	}

	public boolean getState() {
		return state;
	}
	
	public boolean[][] getGrid(){
		return player1.getGrid().getGrid();
	}
	
	public void printOutGrid(){
		 player1.getGrid().printOutGrid();
	}
	
	public SchipType getSchipType() {
		return schipType;
	}

	public void setSchipType(SchipType schipType) {
		this.schipType = schipType;
		noterize();
	}
	
	public void changeValuesGrid(){
		gameState.placeShips(this);
		noterize();
	}
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
		noterize();
	}
	public ArrayList<String> getErrors(){
		return errors;
	}
	public ArrayList<SchipType> getSchips(){
		return schips;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
		noterize();
	}
	public int getNumberOfSchips() {
		return numberOfSchips;
	}
	public void setNumberOfSchips(int numberOfSchips) {
		this.numberOfSchips = numberOfSchips;
		noterize();
	}
	
	public void PlaceShipsComputer(){
		
		for(int k=0; k<5;k++){
			this.schipType = cp.getSchiptype();
			boolean c = true;
			while(c){
				this.x=cp.getX();
				this.y=cp.getY();
				
				this.setState(false);
				if(x+this.schipType.getValue() < 10 && this.checkIfGoodPosition(this.player2.getGrid().getGrid())){
					c=false;	
			}else{
				this.setState(true);
				if(y+this.schipType.getValue() < 10 && this.checkIfGoodPosition(this.player2.getGrid().getGrid())){
					c=false;
			}else{
				cp.SelectNewCoordinates();
				c=true;
			}
			}
				}
			
			int n = this.schipType.getValue();
			int i = 0;
			for( i = 0 ; i < n;i++){
				if(getState()){
					this.player2.getGrid().getGrid()[x][y+i] = true;
				}
			if(!getState()){
				this.player2.getGrid().getGrid()[x+i][y] = true;
				}
			}
			
				cp.SelectNewCoordinates();
				cp.SelectNextShip();
			}
		
		this.setState(false);
	}
	
	public boolean[][] getComputergrid() {
		return this.player2.getGrid().getGrid();
	}
	public  boolean checkIfGoodPosition(boolean[][] grid){
		int n = this.getSchipType().getValue();
		if(getState()){
			for(int i = 0; i < n;i++){
				if(grid[getX()][getY()+i] == true){
					return false;
					}
				
				if(!(getX() == 0))
				{
					if(grid[getX()-1][getY()+i] == true){
						return false;
						}
					
				}
				if(!(getX() >= 9))
				{
					if(grid[getX()+1][getY()+i] == true){
						return false;
						}
					
				}		
				}
			if(!(getY() == 0))
			{
				if(grid[getX()][getY()-1] == true)
				{
					return false;
				}
			}
			if(!(getY()+n > 9))
			{
				if(grid[getX()][getY()+n] == true)
				{
					return false;
				}	
			}
		}
		
		
		
		
		if(!getState()){
			for(int i = 0; i < n;i++){
				if(grid[getX()+i][getY()] == true){
					return false;
					}
				if(!(getY() == 0))
				{
					if(grid[getX()+i][getY()-1] == true){
						return false;
						}
					
				}	
				if(!(getY() == 9))
				{
					if(grid[getX()+i][getY()+1] == true){
						return false;
						}
					}
				
				}	
			if(!(getX() == 0))
			{
				if(grid[getX()-1][getY()] == true)
				{
					return false;
				}
			}
			if(!(getX()+n > 9))
			{
				if(grid[getX()+n][getY()] == true)
				{
					return false;
				}
			}
			
			}
		
		return true;
		}
		
	
	
	public void setGameNew(){
		gameState = new New();

	}
	public void setGameStarted(){
		gameState = new Started();
		
	}
	public GameState getGameState() {
		return gameState;
	}
	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}
	public int getComputerDestroyed() {
		return computerDestroyed;
	}
	public void setComputerDestroyed(int destroyed) {
		this.computerDestroyed = destroyed;
	}
	
	public void destroyComputerShip(){
		computerDestroyed = computerDestroyed + 1;
		if(computerDestroyed == 5)
		{
			winGame();
			
		}
	}
	
	public int getPlayerDestroyed() {
		return playerDestroyed;
	}
	public void setPlayerDestroyed(int destroyed) {
		this.playerDestroyed = destroyed;
	}
	
	public void destroyPlayerShip(){
		playerDestroyed = playerDestroyed + 1;
		if(playerDestroyed == 5)
		{
			gameState.stop(this);
			setChanged();
			notifyObservers("Lost With Computer Score: " + this.getPlayer1().getScore());
			resetGame();
		}
	}
	
	public void winGame(){
		gameState.stop(this);
		setChanged();
		notifyObservers("Won With Score: " + this.getPlayer2().getScore());
		resetGame();
		
	}
	public void resetGame(){
		computer = BehaviourFactory.getBehaviour();
		this.player1.getGrid().reset();
		this.player2.getGrid().reset();
		this.schips.get(0).increase();
		this.schips.get(1).increase();
		this.schips.get(2).increase();
		this.schips.get(3).increase();
		this.schips.get(4).increase();
		this.numberOfSchips = 0;
		this.setComputerDestroyed(0);
		this.setPlayerDestroyed(0);
	}
	
	private void noterize(){
		setChanged();
		notifyObservers("player");
	}
	public ComputerBehaviour getComputer() {
		return computer;
	}
	public void setComputer(ComputerBehaviour computer) {
		this.computer = computer;
	}
	public Place getCurrentPlace(){
		return this.computer.getCurrentPosition();
	}
	public void play(){
		this.computer.play();
		setChanged();
		notifyObservers("computer");
	}
	
	public Player getPlayer1(){
		return this.player1;
	}
	
	public Player getPlayer2(){
		return this.player2;
	}

}