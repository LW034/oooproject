package model.domain;
/**
@author : r0588667 Lennert Wieers,Jeroen Debra

*/
public enum SchipType {

	Vliegdekschip(5,1),
	Slagschip(4,1),
	Onderzeeer(3,1),
	Torpedobootjager(3,1),
	Patrouilleschip(2,1);
	
	private  int value;
	private int amount;
	SchipType( int value,int amount){
		this.value = value;
		this.amount = amount;
	}
	public int getValue(){
		return this.value;
	}
	public String toString(){
		return name()+"("+this.value+")("+getAmount()+")";
	}
	public int getAmount(){
		return this.amount;
	}
	public void decrease(){
	amount--;
	}
	public void increase(){
		amount++;
	}
	
	public SchipType getNext() {
		return values()[(ordinal() + 1) % values().length];
		}
	
}
