package model.domain;
/**
 * 
 * @author : r0588667 Lennert Wieers,Jeroen Debra
 *
 */
import java.util.ArrayList;

import model.behaviour.Place;
import model.states.GameState;

public interface Model {

	void setState(boolean state);
	boolean getState();
	SchipType getSchipType();
	void setSchipType(SchipType schipType);
	void setXY(int x,int y);
	void changeValues();
	ArrayList<String> getErrors();
	boolean[][] getGrid();
	Game getGame();
	void setGameNew();
	void setGameStarted();
	int getNumberSchips();
	GameState getGameState();

	int getDestroyed();
	void setDestroyed(int i);
	void destroyShip();

	int getComputerDestroyed();
	void setComputerDestroyed(int i);
	void destroyComputerShip();

	int getPlayerDestroyed();
	void setPlayerDestroyed(int i);
	void destroyPlayerShip();
	public void changeValuescomputer();
	public boolean[][] getComputerGrid();
	public Place getPlace();
	public void increaseScorePlayer1();
	public void decreaseScorePlayer1();
	public void increaseScorePlayer2();
	public void decreaseScorePlayer2();
	public Player getPlayer1();
	public Player getPlayer2();
}
