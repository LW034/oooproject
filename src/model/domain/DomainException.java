package model.domain;
/**
 * 
 * @author : r0588667 Lennert Wieers
 *
 */
public class DomainException extends RuntimeException {

	public DomainException(){
		super();
	}
	public DomainException(String message){
		super(message);
	}
}
