package model.domain;
/**
 * 
 * @author : r0588667 Lennert Wieers,Jeroen Debra
 *
 */
import java.util.ArrayList;
import java.util.Observable;

import javax.swing.JOptionPane;

import model.behaviour.Place;
import model.states.GameState;

public class GameFacade  implements Model {

	private Game game;
	
	public GameFacade(String spelernaam){
		this.game = new Game(spelernaam);
	}
	@Override
	public void setState(boolean state) {
		game.setState(state);
		
	}

	@Override
	public boolean getState() {
		return game.getState();
	}
	@Override
	public boolean[][] getGrid(){
		return game.getGrid();
	}
	

	@Override
	public SchipType getSchipType() {
		return game.getSchipType();
	}
	@Override
	public void setSchipType(SchipType schipType) {
		game.setSchipType(schipType);

	}

	@Override
	public void setXY(int x,int y) {
		game.setX(x);
		game.setY(y);
		
	}
	@Override
	public void changeValues(){
		game.changeValuesGrid();
		
	}
	@Override
	public ArrayList<String> getErrors(){
		return game.getErrors();
	}
	
	
	@Override
	public void changeValuescomputer() {
		game.PlaceShipsComputer();
	}
	
	public boolean[][] getComputerGrid() {
		return game.getComputergrid();
	}
	@Override
	public void setGameNew() {
		game.setGameNew();
		
	}
	@Override
	public void setGameStarted() {
		game.setGameStarted();
	}
	@Override
	public int getNumberSchips() {
		return game.getNumberOfSchips();
	}
	@Override
	public GameState getGameState() {
		return game.getGameState();
	}
	@Override
	public int getComputerDestroyed() {
		return game.getComputerDestroyed();
	}
	@Override
	public void setComputerDestroyed(int i) {
		game.setComputerDestroyed(i);
		
	}
	@Override
	public void destroyComputerShip() {
		game.destroyComputerShip();
		
	}
	
	@Override
	public int getPlayerDestroyed() {
		return game.getPlayerDestroyed();
	}
	@Override
	public void setPlayerDestroyed(int i) {
		game.setPlayerDestroyed(i);
		
	}
	@Override
	public void destroyPlayerShip() {
		game.destroyPlayerShip();
		
	}
	@Override
	public Game getGame() {
		return game;
	}

	@Override
	public Place getPlace() {
		return game.getComputer().getCurrentPosition();
	}
	@Override
	public void increaseScorePlayer1() {
		game.getPlayer1().increase();
	}
	@Override
	public void decreaseScorePlayer1() {
		game.getPlayer1().decrease();
		
	}
	@Override
	public void increaseScorePlayer2() {
		game.getPlayer2().increase();
		
	}
	@Override
	public void decreaseScorePlayer2() {
		game.getPlayer2().decrease();
		
	}
	@Override
	public Player getPlayer1() {
		return game.getPlayer1();
	}
	
	@Override
	public Player getPlayer2() {
		return game.getPlayer2();
	}
	@Override
	public int getDestroyed() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void setDestroyed(int i) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void destroyShip() {
		// TODO Auto-generated method stub
		
	}

}
