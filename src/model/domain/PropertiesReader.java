/**
 * 
 * @author : r0588667 Lennert Wieers
 *
 */
package model.domain;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

	
	public static Properties getProperties(String filelocation){
		Properties properties = null;
		try{
		properties = new Properties();
		InputStream is = new FileInputStream(filelocation);
		properties.load(is);
		is.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return properties;
	}
}
