/**
 * 
 * @author : r0588667 Jeroen Debra
 *
 */

package model.domain;

import java.util.Arrays;

public class Grid {
	
	private boolean[][] grid;

	public Grid(){
	grid = new boolean[10][10];
	for(boolean row[]: grid){
	    Arrays.fill(row, false);
	}
	}

	public boolean[][] getGrid(){
	return grid;
	}
	public void printOutGrid(){
		   for(int i = 0; i < 10; i++)
		   {
		      for(int j = 0; j < 10; j++)
		      {
		         System.out.print(grid[i][j]+" ");
		      }
		      System.out.println();
		   }
		}
	
	public void reset(){
		this.grid = new boolean[10][10];
	}
}
