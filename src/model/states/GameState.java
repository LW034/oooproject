/**
 * @author : r0581114 Robbe Vaes
 */
package model.states;

import model.domain.Game;

public interface GameState {
	void start(Game game);
	void stop(Game game);
	void placeShips(Game game);
	String toString();
	
	
}
