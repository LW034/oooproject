/**
 * @author : r0581114 Robbe Vaes
 */
package model.states;

import model.domain.DomainException;
import model.domain.Game;

public class New implements GameState {


	@Override
	public void placeShips(Game game) {
		try{
			if(game.getY()+game.getSchipType().getValue() > 10 && game.getState()
					|| game.getX()+game.getSchipType().getValue() > 10 && !game.getState() || !game.checkIfGoodPosition(game.getGrid())){
				throw new DomainException("Invalid Position");
			}
			game.getSchipType().decrease();
			if(game.getSchipType().getAmount() <= -1){
				game.getSchipType().increase();
				throw new DomainException("You can not place any more of this kind");
			}
			if(game.getNumberOfSchips() >= 5){
				throw new DomainException("You Have placed 5 ships");
			}
			else{
			int n = game.getSchipType().getValue();
			int i = 0;
			for( i = 0 ; i < n;i++){
				if(game.getState()){
					game.getGrid()[game.getX()][game.getY()+i] = true;
				}
				if(!game.getState()){
					game.getGrid()[game.getX()+i][game.getY()] = true;
				}
			}	
			}
			game.setNumberOfSchips(game.getNumberOfSchips() + 1);
		}
		catch(Exception e){
			game.getErrors().add(e.getMessage());
		}
		
	}

	@Override
	public void start(Game game) {	
		game.setGameStarted();
		
	}

	@Override
	public void stop(Game game) {
		throw new IllegalArgumentException("Game is not started yet.");
		
	}
	
	@Override
	public String toString()
	{
		return "new";
	}
	
	
}
