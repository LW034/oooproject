/**
 * @author : r0581114 Robbe Vaes,Jeroen Debra
 */
package model.states;

import java.util.Observable;

import model.domain.Game;

public class Started implements GameState {

	@Override
	public void start(Game game) {
		throw new IllegalArgumentException("Game already started");
		
	}

	@Override
	public void placeShips(Game game) {
		
		
	}

	@Override
	public void stop(Game game) {
		game.setGameNew();
		
	}


	@Override
	public String toString()
	{
		return "started";
	}
}
