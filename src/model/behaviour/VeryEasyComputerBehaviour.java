package model.behaviour;
/**
 * @author : r0588667 Lennert Wieers
 */
public class VeryEasyComputerBehaviour extends StandardBehaviour {
	private int x;
	private int y;
	
	@Override
	public void randomMove(){
		this.setOnGrid(x, y);
		x++;
		check();
	}
	@Override
	public void afterHit() {
		this.randomMove();
		
	}

	@Override
	public void afterMultipleHits() {
		this.randomMove();
		
	}
	private void check(){
		if(x == 10){
			y++;
			x = 0;
		}
		else if(y == 10){
			y = 0;
		}
	}

}
