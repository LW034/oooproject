package model.behaviour;
/**
 * @author : r0588667 Lennert Wieers
 */
import model.domain.Game;

public interface ComputerBehaviour {

	public void randomMove();
	public void afterHit();
	public void afterMultipleHits();
	public void setGame(Game game);
	public Place getCurrentPosition();
	public void play();
}
