package model.behaviour;
/**
 * @author : r0588667 Lennert Wieers
 */
public class EasyComputerBehaviour extends StandardBehaviour {

	public EasyComputerBehaviour() {
		super();
	}
	@Override
	public void afterHit() {
		this.randomMove();
		
	}
	@Override
	public void afterMultipleHits() {
		this.randomMove();
		
	}


}
