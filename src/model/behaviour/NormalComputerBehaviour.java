package model.behaviour;
/**
 * @author : r0588667 Lennert Wieers
 */
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

public class NormalComputerBehaviour extends StandardBehaviour {

	public ArrayList<Place> getPossiblePlaces(){
		ArrayList<Place> result = new ArrayList<>();
		Place toAdd = null;
		if(getCurrentPosition().getX()-1 < 10 && getCurrentPosition().getX()-1 >= 0 && !this.getPlace(getCurrentPosition().getX()-1,getCurrentPosition().getY()).getChecked()){
			toAdd = new Place(getCurrentPosition().getX()-1,getCurrentPosition().getY());
		result.add(toAdd);
		}
		if(getCurrentPosition().getX()+1 < 10 && getCurrentPosition().getX()+1 >= 0 && !this.getPlace(getCurrentPosition().getX()+1,getCurrentPosition().getY()).getChecked()){

			toAdd = new Place(getCurrentPosition().getX()+1,getCurrentPosition().getY());
			result.add(toAdd);
		}
		if(getCurrentPosition().getY()-1 < 10 && getCurrentPosition().getY()-1 >= 0 && !this.getPlace(getCurrentPosition().getX(),getCurrentPosition().getY()-1).getChecked()){
			toAdd = new Place(getCurrentPosition().getX(),getCurrentPosition().getY()-1);
			result.add(toAdd);
		}
		if(getCurrentPosition().getY()+1 < 10 && getCurrentPosition().getY()+1 >= 0 && !this.getPlace(getCurrentPosition().getX(),getCurrentPosition().getY()+1).getChecked()){
			toAdd = new Place(getCurrentPosition().getX(),getCurrentPosition().getY()+1);
			result.add(toAdd);
		}
		
		return result;
	}
public void afterHit(){
	ArrayList<Place> positions = this.getPossiblePlaces();
	if(positions.size() == 0){
		this.randomMove();
	}
	else {
		Random rand = new Random();
		int randomNum = rand.nextInt((positions.size()-1)+1)+0;
		Place p = positions.get(randomNum);
		this.setOnGrid(p.getX(), p.getY());
	}
}

public void afterMultipleHits(){
int x = getCurrentPosition().getX() - getLastPosition().getX();
int y = getCurrentPosition().getY() - getLastPosition().getY();
if(x == 0){
	if(getCurrentPosition().getY()+y < 10 && getCurrentPosition().getY()+y >= 0){
		if(!getPlace(getCurrentPosition().getX()+x,getCurrentPosition().getY()+y).getChecked()){
			this.setOnGrid(getCurrentPosition().getX()+x,getCurrentPosition().getY()+y);
		}
		else{
			this.randomMove();
		}
	}
	else {
		if(!getPlace(getCurrentPosition().getX()-x,getCurrentPosition().getY()-y).getChecked()){
			this.setOnGrid(getCurrentPosition().getX()-x,getCurrentPosition().getY()-y);
		}
		else{
			this.randomMove();
		}
		
	}
}
if(y == 0){
	if(getCurrentPosition().getX()+x < 10 && getCurrentPosition().getX()+x >= 0){
		if(!getPlace(getCurrentPosition().getX()+x,getCurrentPosition().getY()+y).getChecked()){
			this.setOnGrid(getCurrentPosition().getX()+x,getCurrentPosition().getY()+y);
		}
		else {
			this.randomMove();
		}
	}
	else{
		if(!getPlace(getCurrentPosition().getX()-x,getCurrentPosition().getY()-y).getChecked()){
			this.setOnGrid(getCurrentPosition().getX()-x,getCurrentPosition().getY()-y);
		}
		else{
			this.randomMove();
		}
	}
}
}

}
