package model.behaviour;
/**
 * @author : r0588667 Lennert Wieers
 */
public class Place {
private boolean state,checked;
private int x,y;

public Place(int x,int y){
	this.setX(x);
	this.setY(y);
}

public boolean isHit() {
	return state;
}

public void setHit(Boolean state) {
	this.state = state;
}

public Boolean getChecked() {
	return checked;
}

public void setChecked(boolean checked) {
	this.checked = checked;
}

public void setEntireState(boolean state){
	this.setHit(state);
	this.setChecked(true);
}
public String toString(){
	return "S:"+isHit()+"|"+"C:"+getChecked()+" ("+getX()+", "+getY()+")";
}

public int getY() {
	return y;
}

public void setY(int y) {
	this.y = y;
}

public int getX() {
	return x;
}

public void setX(int x) {
	this.x = x;
}
@Override
public boolean equals(Object o){
	if(o instanceof Place){
		Place p = (Place) o;
		if(this.getX() == p.getX() && this.getY() == p.getY() && this.getChecked() == p.getChecked()){
			return true;
		}
	}
	return false;
}
}
