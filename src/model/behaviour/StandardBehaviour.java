	package model.behaviour;
/**
* @author : r0588667 Lennert Wieers
*/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Random;

import model.domain.Game;
import model.domain.SchipType;

public abstract class StandardBehaviour extends Observable implements ComputerBehaviour{

	
	private Game game;
	private Place lastPosition,previousPosition,currentPosition;
	private Place[][] knowngrid;
	private ArrayList<SchipType> schips;
	public StandardBehaviour() {
		setUp();
	}
	private void setUp(){
		schips = new ArrayList<SchipType>(Arrays.asList(SchipType.values()));
		knowngrid = null;
		lastPosition = new Place(22, 33);
		previousPosition = new Place(11, 22);
		currentPosition = new Place(17, 16);
		knowngrid = new Place[10][10];
		for(int i = 0; i < 10;i++){
			for(int j = 0;j < 10;j++){
				knowngrid[i][j] = new Place(i, j);
			}
		}
	}
	@Override
	public void randomMove() {
	try{
		Random randx = new Random();
		Random randy = new Random();
		int randomNumx = randx.nextInt((9-0)+1)+0;
		int randomNumy = randy.nextInt((9-0)+1)+0;
		if(!getPlace(randomNumx, randomNumy).getChecked()){
			this.setOnGrid(randomNumx, randomNumy);
		}
		else {
			this.randomMove();
		}
	}
	catch(Error e){
		for(int i = 0; i < 10;i++){
			for(int j = 0;j < 10;j++){
				if(!getPlace(i, j).getChecked()){
					this.setOnGrid(i, j);
				}
			}
		}
	}
	}
	public final void play(){

		if(getCurrentPosition().isHit() && getLastPosition().isHit()){
			afterMultipleHits();
		}
		else if(getPreviousPosition().isHit() && getLastPosition().isHit() && !getCurrentPosition().isHit()){
			this.setLast(new Place(11, 22));
			this.setCurrentPosition(new Place(22, 33));
			randomMove();
		}
		else if(getLastPosition().isHit() && !getCurrentPosition().isHit()){
			setCurrentPosition(getLastPosition());
			setLast(getPreviousPosition());
			afterHit();
		}
		else if(getCurrentPosition().isHit()){
			afterHit();
		}
		else if(!getCurrentPosition().isHit()){
			this.randomMove();
		}
		else{
			afterHit();
		}
	}
	public Game getGame(){
		return game;
	}
	@Override
	public void setGame(Game game){
		this.game = game;
	}
	public Place[][] getKnownGrid(){
		return knowngrid;
	}
	public Place getPlace(int x,int y){
		return knowngrid[x][y];
	}
	public Place getLastPosition(){
		return lastPosition;
	}
	
	public void setOnGrid(int x,int y){
		boolean[][] test = game.getGrid();
		boolean state = test[x][y];
		Place p = new Place(x,y);
		p.setEntireState(state);
		this.setPreviousPosition(getLastPosition());
		this.setLast(getCurrentPosition());
		this.setCurrentPosition(p);
		knowngrid[x][y] = p;
	}
	public Place getPreviousPosition(){
		return previousPosition;
	}
	public void setPreviousPosition(Place previousPosition){
		this.previousPosition = previousPosition;
	}
	public void setLast(Place p){
		lastPosition = p;
	}
	public Place getCurrentPosition(){
		return currentPosition;
	}
	public void setCurrentPosition(Place place){
		this.currentPosition = place;
	}
	public ArrayList<SchipType> getSchips(){
		return schips;
	}
	public abstract void afterHit();
	public abstract void afterMultipleHits();
}
