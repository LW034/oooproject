package model.behaviour;
/**
 * @author : r0588667 Lennert Wieers
 */
import java.util.Properties;
import java.util.Random;

import model.domain.PropertiesReader;

public class BehaviourFactory {

	public static ComputerBehaviour getBehaviour(){
		ComputerBehaviour result = null;
		try{
			Properties properties = PropertiesReader.getProperties("ComputerBehaviour.properties");
			Random rand = new Random();
			int randomNum = rand.nextInt((properties.size()-1)+1)+1;
			String type = (String) properties.getProperty(String.valueOf(randomNum));
			result = (ComputerBehaviour) Class.forName(type).newInstance();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
}
