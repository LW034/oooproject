/**
 * 
 * @author : r0588667 Jeroen Debra
 *
 */
package model.placement;

import model.domain.SchipType;

public class StaticComputerPlacement implements ComputerPlacement {
	
	private int x;
	private int y;
	private SchipType schiptype;
	
	
	public StaticComputerPlacement(){
		x=1;
		y=1;	
		schiptype = SchipType.Vliegdekschip;
	}
	
	public void SelectNextShip(){
		this.schiptype = this.schiptype.getNext();
		
	}
	
	public int getY() {
		return y;
	}
	public int getX() {
		return x;
	}
	
	public SchipType getSchiptype() {
		return schiptype;
	}

	@Override
	public String toString() {
		return "StaticComputerPlacement [x=" + x + ", y=" + y + ", schiptype=" + schiptype + "]";
	}

	@Override
	public void SelectNewCoordinates() {
		this.y = y + 2;
		
	}
	

}
