/**
 * 
 * @author : r0588667 Jeroen Debra
 *
 */
package model.placement;

import java.util.Properties;

import model.domain.PropertiesReader;

public class PlacementFactory {

	Properties prop = PropertiesReader.getProperties("Placement.properties");
		  
	 public ComputerPlacement  createPlacementStrategy(){
		 
		 ComputerPlacement CP;
		   if(prop.getProperty("strategy").equals("random")){
			   CP = new RandomComputerPlacement();
		   }else if(prop.getProperty("strategy").equals("static")){
			   CP = new StaticComputerPlacement();
		   }else throw new IllegalArgumentException();
		   return CP;
	}

}
