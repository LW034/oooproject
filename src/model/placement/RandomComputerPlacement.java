/**
 * 
 * @author : r0588667 Jeroen Debra
 *
 */
package model.placement;

import java.util.Random;

import model.domain.SchipType;

public class RandomComputerPlacement implements ComputerPlacement {
	
	private int x;
	private int y;
	private SchipType schiptype;
	private Random random = new Random();
	
	
	
	public RandomComputerPlacement(){
		x= random.nextInt(10);
		y= random.nextInt(10);	
		schiptype = SchipType.Vliegdekschip;
	}
	
	@Override
	public void SelectNextShip(){
		this.schiptype = this.schiptype.getNext();
		
	}
	
	@Override
	public int getY() {
		return y;
	}
	@Override
	public int getX() {
		return x;
	}
	@Override
	public SchipType getSchiptype() {
		return schiptype;
	}

	@Override
	public String toString() {
		return "StaticComputerPlacement [x=" + x + ", y=" + y + ", schiptype=" + schiptype + "]";
	}

	@Override
	public void SelectNewCoordinates() {
		x= random.nextInt(10);
		y= random.nextInt(10);	
	}

}
