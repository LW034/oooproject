/**
 * 
 * @author : r0588667 Jeroen Debra
 *
 */
package model.placement;

import model.domain.SchipType;

public interface ComputerPlacement {
	
	public void SelectNextShip();
	public void SelectNewCoordinates();
	public int getY();
	public int getX();
	public SchipType getSchiptype();


}
