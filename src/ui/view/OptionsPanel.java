package ui.view;
/**
 * 
 * @author : r0588667 Lennert Wieers
 *
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
/**
 * @author : r0588667 Lennert Wiers
 */
import java.awt.GridLayout;
import java.util.Observable;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;

import model.domain.SchipType;
import ui.controller.Controller;
import ui.controller.RadioButtonListener;
import ui.controller.StartButtonListener;

public class OptionsPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Controller controller;
	private JPanel top = new JPanel();
	private JPanel bottom = new JPanel();
	private JRadioButton radhor = new JRadioButton("Horizontaal");
	private JRadioButton radver = new JRadioButton("Vertikaal");
	private JButton start = new JButton("Start");
	JComboBox<SchipType> combo;
	private ButtonGroup butgroup;
	public OptionsPanel(){
		combo = new JComboBox<>(SchipType.values());
		butgroup = new ButtonGroup();
		butgroup.add(radhor);
		butgroup.add(radver);
		init();
	}
	
	private void init(){
		this.setLayout(new GridLayout(2, 0));
		this.setPreferredSize(new Dimension(400, 400));
		JPanel wrap = new JPanel(new GridLayout(2, 1));
		this.top.setLayout(new GridLayout(2,1));
		this.bottom.setLayout(new BorderLayout());
		JLabel top1 = new JLabel("Beschikbare schepen : ");
		combo.setBackground(Color.WHITE);
		combo.setPreferredSize(new Dimension(150, 20));
		JPanel wrapper = new JPanel();
		wrapper.add(combo);;
		this.top.add(top1);
		this.top.add(wrapper);
		wrap.add(top);
		JLabel bot1 = new JLabel("Richting : ");
		this.bottom.add(bot1,BorderLayout.PAGE_START);
		radver.setSelected(true);
		this.bottom.add(radhor,BorderLayout.LINE_START);
		this.bottom.add(radver,BorderLayout.LINE_END);

		wrap.add(bottom);
		this.add(wrap);
		this.add(start);
		start.setEnabled(false);

	}
	public boolean getHorizonalstate(){
		return radhor.isSelected();
	}
	public boolean getVertikalstate(){
		return radver.isSelected();
	}
	public void addIsRadioButtonListener(RadioButtonListener listener,JRadioButton radiobutton){
		radiobutton.addActionListener(listener);
	}
	
	public void addStartListener(StartButtonListener listener,JButton jButton){
		jButton.addActionListener(listener);
	}
	
	

	public Controller getController(){
		return controller;
	}
	public JRadioButton getHorizontalButton(){
		return radhor;
	}
	public JRadioButton getVertikalButton(){
		return radver;
	}
	public JComboBox<SchipType> getComboBox(){
		return combo;
	}
	public SchipType getSchipType(){
		return (SchipType) combo.getSelectedItem();
	}
	public void setComboBox(JComboBox<SchipType> combo){
		this.combo = combo;
	}
	public JButton getStartButton(){
		return start;
	}
	
}
