package ui.view;
/**
 * 
 * @author : r0588667 Lennert Wieers,Jeroen Debra
 *
 */
import java.awt.GridBagLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.domain.SchipType;
import ui.controller.AttackButtonListener;
import ui.controller.Controller;
import ui.controller.RadioButtonListener;
import ui.controller.StartButtonListener;
import ui.controller.TileButtonListener;

import java.awt.Dialog;


public class MainView extends JFrame  implements View,Observer {
	private static final long serialVersionUID = 1L;
	private Controller controller;
	private OptionsPanel options;
	private JPanel container;
	private PlayerGridPanel player,computer;
	
	public MainView(String playerName){
		initMain(playerName);
		initPanels();
	} 
	
		private void initMain(String playerName){
			this.setTitle("Zeeslag");
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setSize(900, 500);
			this.setResizable(false);
			container = new JPanel(new GridBagLayout());
			options = new OptionsPanel();
			player = new PlayerGridPanel("Player : "+playerName);
			computer = new PlayerGridPanel("Computer");
			
		}
		private void initPanels(){
			container.add(options);
			container.add(player);
			container.add(computer);
			this.add(container);
		}

		@Override
		public void update(Observable o, Object arg) {
			if(arg instanceof String){
				if(arg == "player"){
					player.setHorizontal(getController().getRadioButtonState());
					player.setError(getController().getErrors());
					player.color(getController().getGrid());
				}
				if(arg == "computer"){
					if(getController().getPlace().getX()< 10 
							&& getController().getPlace().getY() < 10){
						player.guessColor(getController().getPlace());
					}
				}
				if( arg.toString().charAt(0) == 'W'){
					winGame(arg);
				}
				if(arg.toString().charAt(0) == 'L'){
					loseGame(arg);
				}
			}
		}
		
		@Override
		public void winGame(Object arg)
		{
			JOptionPane.showMessageDialog(null, "You " + arg.toString());

	        resetGrids();
		}
		@Override
		public void loseGame(Object arg)
		{
			JOptionPane.showMessageDialog(null, "You " + arg.toString());
	        resetGrids();

		}
		@Override
		public void setController(Controller controller) {
			this.controller = controller;
			
		}
		private Controller getController(){
			return controller;
		}


		@Override
		public boolean getRadioButtonState() {
			return options.getHorizonalstate();
		}


		@Override
		public void start() {
			this.setVisible(true);
			
		}


		@Override
		public void addIsListener(RadioButtonListener listener) {
			options.addIsRadioButtonListener(listener,options.getHorizontalButton());
			options.addIsRadioButtonListener(listener,options.getVertikalButton());
		}


		@Override
		public void addIsTileButtonListener(TileButtonListener listener) {
			player.addTileButtonListener(listener);
			
		}


		@Override
		public SchipType getSchipType() {
			return options.getSchipType();
		}
		
		@Override
		public void PlaceComputerShips(boolean[][] grid){
			computer.color(grid);
		}


		@Override
		public void addStartButtonListener(StartButtonListener listener) {
			options.addStartListener(listener, options.getStartButton());
			
		}


		@Override
		public JButton getStartButton() {
			return options.getStartButton();
		}
		
		public PlayerGridPanel getPlayerPanel(){
			return player;
		}
		
		public PlayerGridPanel getComputerPanel(){
			return computer;
		}


		@Override
		public JButton[][] getPlayerButtons() {		
			return player.getButtons();
		}


		@Override
		public JButton[][] getComputerButtons() {
			return computer.getButtons();
		}


		@Override
		public void addAttackButtonListener(AttackButtonListener listener) {
			computer.addAttackButtonListener(listener);
			
		}
		@Override
		public void resetGrids()
		{
			player.resetGrid();
			computer.resetGrid();
		}
		public void setName(String playerName){
			player.setName(playerName);
		}

}
