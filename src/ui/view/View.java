package ui.view;
/**
 * 
 * @author : r0588667 Lennert Wieers,Robbe Vaes
 *
 */
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;

import model.domain.SchipType;
import ui.controller.AttackButtonListener;
import ui.controller.Controller;
import ui.controller.RadioButtonListener;
import ui.controller.StartButtonListener;
import ui.controller.TileButtonListener;

public interface View  {

	void setController(Controller controller);
	boolean getRadioButtonState();
	SchipType getSchipType();
	void start();
	void addIsListener(RadioButtonListener listener);
	void addIsTileButtonListener(TileButtonListener listener);
	void addStartButtonListener(StartButtonListener listener);
	void addAttackButtonListener(AttackButtonListener listener);
	JButton getStartButton();
	PlayerGridPanel getPlayerPanel();
	PlayerGridPanel getComputerPanel();
	JButton[][] getPlayerButtons();
	JButton[][] getComputerButtons();
	void PlaceComputerShips(boolean[][] grid);

	void resetGrids();
	void setName(String playerName);
	void winGame(Object arg);
	void loseGame(Object arg);

	
	
}
