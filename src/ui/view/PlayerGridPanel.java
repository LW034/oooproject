package ui.view;
/**
 * 
 * @author : r0588667 Lennert Wieers,Jeroen Debra
 *
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import model.behaviour.Place;
import model.domain.SchipType;
import ui.controller.AttackButtonListener;
import ui.controller.Controller;
import ui.controller.TileButtonListener;

public class PlayerGridPanel extends JPanel {
	
	private boolean horizontal;
	private ArrayList<String> errors;
	private JButton[][] button = new JButton[10][10];
	private JLabel pl;
    public PlayerGridPanel(String playerName){
    	errors = new ArrayList<String>();
    	init(playerName);
    }
    	private void init(String name){
    		this.setLayout(new BorderLayout());
    		this.setPreferredSize(new Dimension(300, 400));
    		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
    		TitledBorder border = BorderFactory.createTitledBorder(loweredetched);
    		border.setTitleJustification(TitledBorder.RIGHT);	
    		this.pl = new JLabel(name);

    			JPanel p = new JPanel(new GridLayout(10, 10));
    			p.setBorder(loweredetched);
    			p.setMinimumSize(new Dimension(300, 400));
    			for(int i=0;i<10;i++)
    			{
    			     for(int j=0;j<10;j++)
    			     {
    			        button[i][j] = new JButton();
    			        button[i][j].setOpaque(true);
    			        p.add(button[i][j]);
    			     }
    			}
    		this.add(pl,BorderLayout.PAGE_START);
    		this.add(p,BorderLayout.CENTER);
    		}
   
		public boolean isHorizontal() {
			return horizontal;
		}
		public void setHorizontal(boolean horizontal) {
			this.horizontal = horizontal;
		}

		public void addTileButtonListener(TileButtonListener listener){
			for(int i=0;i<10;i++)
			{
			     for(int j=0;j<10;j++)
			     {
			    	button[i][j].putClientProperty("X-As",i);
			    	button[i][j].putClientProperty("Y-As",j);
			        button[i][j].addActionListener(listener);
			     }
			}
		}
		
		public void addAttackButtonListener(AttackButtonListener listener){
			for(int i=0;i<10;i++)
			{
			     for(int j=0;j<10;j++)
			     {
			    	button[i][j].putClientProperty("X-As",i);
			    	button[i][j].putClientProperty("Y-As",j);
			        button[i][j].addActionListener(listener);
			     }
			}
		}
		public void color(boolean[][] grid){
			for (int i = 0; i < 10;i++){
				for(int j = 0; j <10;j++){
					if(grid[i][j] == true){
						button[i][j].setBackground(Color.green);
					}
				}
			}
		}
		public void guessColor(Place p){
			if(p.isHit()){
					button[p.getX()][p.getY()].setBackground(Color.yellow);
				}
				else {
					button[p.getX()][p.getY()].setBackground(Color.black);
				}
		}
		public void setError(ArrayList<String> errors){
			this.errors = errors;
			for(String message : this.errors ){
				JOptionPane.showMessageDialog(null, message);
			}
			errors.clear();
		}
		public JButton[][] getButtons(){
			return button;
		}
		
		public void resetGrid(){
			for(int i=0;i<10;i++)
			{
			     for(int j=0;j<10;j++)
			     {
			    	 button[i][j].setBackground(null); 
			    	 button[i][j].setEnabled(true);

			     }
			}
		}
		
		public void setPlayerLabel(String naam, int score){
			this.pl.setText(naam + "   (" + score + ")");
		}
		
    }
