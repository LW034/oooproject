/**
 * @author : r0581114 Robbe Vaes
 */

package ui.controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import model.domain.Model;
import model.domain.SchipType;
import ui.view.View;

public class StartButtonListener extends ControllerCommon implements ActionListener {

	public StartButtonListener(View view, Model model) {
		super(view, model);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.getModel().changeValuescomputer();
		
		this.getModel().getPlayer1().ScoreReset();
		this.getModel().getPlayer2().ScoreReset();
		
		// berekent en toont begin score speler1
		for(int i=0;i<10;i++)
		{
		     for(int j=0;j<10;j++)
		     {
		       
		      if(this.getModel().getGrid()[i][j] == true){
		    	  getModel().increaseScorePlayer1();
		      }
		    	 
		     }}
		this.getView().getPlayerPanel().setPlayerLabel(this.getModel().getPlayer1().getName(), this.getModel().getPlayer1().getScore() );
		
		for(int i=0;i<10;i++)
		{
		     for(int j=0;j<10;j++)
		     {
		       
		      if(this.getModel().getComputerGrid()[i][j]==true){
		    	  getModel().increaseScorePlayer2();
		      }
		    	 
		     }}
		this.getView().getComputerPanel().setPlayerLabel(this.getModel().getPlayer2().getName(), this.getModel().getPlayer2().getScore() );
		
		
		
		try{
			getModel().setGameStarted();
			getView().getStartButton().setEnabled(false);
			for(int i=0;i<10;i++)
			{
			     for(int j=0;j<10;j++)
			     {
			    	getView().getPlayerPanel().getButtons()[i][j].setEnabled(false);
			    	getView().getComputerPanel().getButtons()[i][j].setBackground(null);
			     }
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
