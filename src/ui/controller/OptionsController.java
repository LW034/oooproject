package ui.controller;
/**
 * 
 * @author : r0588667 Lennert Wieers
 *
 */
import java.util.ArrayList;

import model.behaviour.Place;
import model.domain.Model;
import model.domain.SchipType;
import ui.view.View;

public class OptionsController extends ControllerCommon implements Controller {

	
	public OptionsController(View view, Model model) {
		super(view, model);
		getView().setController(this);
	}
	@Override
	public boolean getRadioButtonState(){
		return this.getModel().getState();
	}
	@Override
	public void startView() {
		this.getView().start();
		
	}
	@Override
	public boolean[][] getGrid() {
		return this.getModel().getGrid();
	}
	@Override
	public SchipType getSchipType() {
		return this.getModel().getSchipType();
	}
	@Override
	public void changeValues(){
		this.getModel().changeValues();
	}
	@Override
	public ArrayList<String> getErrors() {
		return this.getModel().getErrors();
		
	}
	@Override
	public boolean[][] getComputerGrid() {
		getModel().changeValuescomputer();
		return getModel().getComputerGrid();
	}
	@Override
	public Place getPlace() {
		return getModel().getPlace();
	}
}
