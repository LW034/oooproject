/**
 * @author : r0581114 Robbe Vaes,Jeroen Debra, Lennder Wieers
 */
package ui.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import model.domain.Model;
import ui.view.View;

public class AttackButtonListener extends ControllerCommon implements ActionListener {

	public AttackButtonListener(View view, Model model) {
		super(view, model);
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		int x = (Integer)((JButton)a.getSource()).getClientProperty( "X-As" );
		int y = (Integer)((JButton)a.getSource()).getClientProperty("Y-As");
		try{
			if(getModel().getGameState().toString().equals("started"))
			{
				getModel().getGame().getComputer().setGame(getModel().getGame());
				getModel().getGame().play();
				//zet zwart waar geklikt
				getView().getComputerButtons()[x][y].setBackground(Color.black	);
				getView().getComputerButtons()[x][y].setEnabled(false);
				if(getModel().getComputerGrid()[x][y])
				{
					//als het een schip is, zet op geel
					getView().getComputerButtons()[x][y].setBackground(Color.YELLOW	);
					if(getModel().getComputerGrid()[x][y])
					{
						//verander de score van player1
						this.getModel().decreaseScorePlayer1();
						this.getView().getPlayerPanel().setPlayerLabel(this.getModel().getPlayer1().getName(), this.getModel().getPlayer1().getScore());
						
						
						boolean destroyed = true;
						int i = 0;
						//gaat na naar alle richtingen, omhoog, omlaag, links, rechts, of alle true's die aanliggend zijn geel zijn
						//zo ja zet laat destroyed op true
						//anders destroyed op false
						while((x+i < 10) && getModel().getComputerGrid()[x+i][y] && destroyed )
						{
							
							if(!(getView().getComputerButtons()[x+i][y].getBackground().equals(Color.YELLOW)))
							{
								destroyed = false;
							}
							i++;
						}
						i = 0;
						while((x-i > -1) && getModel().getComputerGrid()[x-i][y] && destroyed )
						{
							if(!(getView().getComputerButtons()[x-i][y].getBackground().equals(Color.YELLOW)))
							{
								destroyed = false;
							}
							i++;
						}
						i = 0;
						while((y+i < 10) && getModel().getComputerGrid()[x][y+i] && destroyed  )
						{
							if(!(getView().getComputerButtons()[x][y+i].getBackground().equals(Color.YELLOW)))
							{
								destroyed = false;
							}
							i++;
						}
						i = 0;
						while((y-i > -1) && getModel().getComputerGrid()[x][y-i] && destroyed )
						{
							if(!(getView().getComputerButtons()[x][y-i].getBackground().equals(Color.YELLOW)))
							{
								destroyed = false;
							}
							i++;
						}
						i = 0;
						
						
						//als destroyed op true blijft, ga weer alle richtingen af voor aanliggende vakjes van schip, zet deze op rood
						if(destroyed)
						{
							
							
							getView().getComputerButtons()[x][y].setBackground(Color.RED);
							while((x+i < 10) && getModel().getComputerGrid()[x+i][y] && destroyed )
							{
								getView().getComputerButtons()[x+i][y].setBackground(Color.RED);
								i++;
							}
							i = 0;
							while((x-i > -1) && getModel().getComputerGrid()[x-i][y] && destroyed )
							{
								getView().getComputerButtons()[x-i][y].setBackground(Color.RED);
								i++;
							}
							i = 0;
							while((y+i < 10) && getModel().getComputerGrid()[x][y+i] && destroyed  )
							{
								getView().getComputerButtons()[x][y+i].setBackground(Color.RED);
								i++;
							}
							i = 0;
							while((y-i > -1) && getModel().getComputerGrid()[x][y-i] && destroyed )
							{
								getView().getComputerButtons()[x][y-i].setBackground(Color.RED);
								i++;
							}
							i = 0;
							getModel().destroyComputerShip();
						}
					}
					
				}
				
				// VOOR PLAYERGRID
				 x = getModel().getPlace().getX();
				 y = getModel().getPlace().getY();
				if(getModel().getGrid()[x][y])
				{
					
				
					this.getModel().decreaseScorePlayer2();
					this.getView().getComputerPanel().setPlayerLabel(this.getModel().getPlayer2().getName(), this.getModel().getPlayer2().getScore());
					
					
					boolean destroyed = true;
					int i = 0;
					//gaat na naar alle richtingen, omhoog, omlaag, links, rechts, of alle true's die aanliggend zijn geel zijn
					//zo ja zet laat destroyed op true
					//anders destroyed op false
					while((x+i < 10) && getModel().getGrid()[x+i][y] && destroyed )
					{
						if(!(getView().getPlayerButtons()[x+i][y].getBackground().equals(Color.YELLOW)))
						{
							destroyed = false;
						}
						i++;
					}
					i = 0;
					while((x-i > -1) && getModel().getGrid()[x-i][y] && destroyed )
					{
						if(!(getView().getPlayerButtons()[x-i][y].getBackground().equals(Color.YELLOW)))
						{
							destroyed = false;
						}
						i++;
					}
					i = 0;
					while((y+i < 10) && getModel().getGrid()[x][y+i] && destroyed  )
					{
						if(!(getView().getPlayerButtons()[x][y+i].getBackground().equals(Color.YELLOW)))
						{
							destroyed = false;
						}
						i++;
					}
					i = 0;
					while((y-i > -1) && getModel().getGrid()[x][y-i] && destroyed )
					{
						if(!(getView().getPlayerButtons()[x][y-i].getBackground().equals(Color.YELLOW)))
						{
							destroyed = false;
						}
						i++;
					}
					i = 0;
					
					
					//als destroyed op true blijft, ga weer alle richtingen af voor aanliggende vakjes van schip, zet deze op rood
					if(destroyed)
					{
						
						
						getView().getPlayerButtons()[x][y].setBackground(Color.RED);
						while((x+i < 10) && getModel().getGrid()[x+i][y] && destroyed )
						{
							getView().getPlayerButtons()[x+i][y].setBackground(Color.RED);
							i++;
						}
						i = 0;
						while((x-i > -1) && getModel().getGrid()[x-i][y] && destroyed )
						{
							getView().getPlayerButtons()[x-i][y].setBackground(Color.RED);
							i++;
						}
						i = 0;
						while((y+i < 10) && getModel().getGrid()[x][y+i] && destroyed  )
						{
							getView().getPlayerButtons()[x][y+i].setBackground(Color.RED);
							i++;
						}
						i = 0;
						while((y-i > -1) && getModel().getGrid()[x][y-i] && destroyed )
						{
							getView().getPlayerButtons()[x][y-i].setBackground(Color.RED);
							i++;
						}
						i = 0;
						getModel().destroyPlayerShip();
					}
				}
				
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
