package ui.controller;
/**
 * 
 * @author : r0588667 Lennert Wieers
 *
 */
import java.util.ArrayList;

import model.behaviour.Place;
import model.domain.SchipType;

public interface Controller {

	boolean[][] getGrid();
	boolean[][] getComputerGrid();
	public Place getPlace();
	boolean getRadioButtonState();
	SchipType getSchipType();
	void changeValues();
	void startView();
	ArrayList<String> getErrors();
}
