package ui.controller;
/**
 * 
 * @author : r0588667 Lennert Wieers
 *
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.domain.Model;
import model.domain.SchipType;
import ui.view.View;

public class RadioButtonListener extends ControllerCommon implements ActionListener {

	public RadioButtonListener(View view, Model model) {
		super(view, model);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		boolean state = getView().getRadioButtonState();
		try{
			getModel().setState(state);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
