package ui.controller;
/**
 * 
 * @author : r0588667 Lennert Wieers
 *
 */
import model.domain.Model;
import ui.view.View;

public class ControllerCommon {

	private View view;
	private Model model;
	
	public ControllerCommon(View view,Model model){
		this.setView(view);
		this.setModel(model);
	}

	protected View getView() {
		return view;
	}

	private void setView(View view) {
		this.view = view;
	}

	protected Model getModel() {
		return model;
	}

	private void setModel(Model model) {
		this.model = model;
	}
}
