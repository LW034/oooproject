package ui.controller;
/**
 * 
 * @author : r0588667 Lennert Wieers
 *
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import model.domain.Model;
import model.domain.SchipType;
import ui.view.View;

public class TileButtonListener extends ControllerCommon implements ActionListener {

	public TileButtonListener(View view, Model model) {
		super(view, model);
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		SchipType schipType = getView().getSchipType();
		int x = (Integer)((JButton)a.getSource()).getClientProperty( "X-As" );
		int y = (Integer)((JButton)a.getSource()).getClientProperty("Y-As");
		try{
			getModel().setSchipType(schipType);
			getModel().setXY(x, y);
			getModel().changeValues();
			if(getModel().getNumberSchips() == 5 && !(getModel().getGameState().toString().equals("started")))
			{
				getView().getStartButton().setEnabled(true);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
