package application;
/**
 * @author : r0588667 Lennert Wieers,Jeroen Debra
 */

import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import model.domain.GameFacade;
import model.domain.MediaPlayer;
import model.domain.Model;
import model.domain.SchipType;
import ui.controller.AttackButtonListener;
import ui.controller.Controller;
import ui.controller.OptionsController;
import ui.controller.RadioButtonListener;
import ui.controller.StartButtonListener;
import ui.controller.TileButtonListener;
import ui.view.MainView;
import ui.view.View;

public class Launcher {

	public static void main(String[] args) {
		String playerName = JOptionPane.showInputDialog("Name of the Player?");
		Model m = new GameFacade(playerName);
		if(playerName == null || playerName.isEmpty()){
			System.exit(0);
		}
		else {
		View v = new MainView(playerName);
		//MediaPlayer.playMusic();
		m.getGame().addObserver((Observer) v);
		Controller c;
		RadioButtonListener al;
		TileButtonListener tl;
		StartButtonListener sl;
		AttackButtonListener atl;
		try{
			al = new RadioButtonListener(v, m);
			tl = new TileButtonListener(v, m);
			sl = new StartButtonListener(v,m);
			atl = new AttackButtonListener(v,m);
			v.addIsListener(al);
			v.addIsTileButtonListener(tl);
			v.addStartButtonListener(sl);
			v.addAttackButtonListener(atl);
			c = new OptionsController(v, m);
			c.startView();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		}
	}

}
