package test;

import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import model.domain.Game;
import model.domain.GameFacade;
import static org.mockito.Mockito.verify;
public class FacadeTest {

	private GameFacade gamefacade;
	@Mock
	private Game game;
	
	@Before
	public void setUp() throws Exception {
		initMocks(this);
		gamefacade = new GameFacade(game);
	}
	
	@Test
	public void test_changeValues_changes_the_values_if_no_errors_are_thrown(){
		gamefacade.changeValues();
		verify(game).changeValuesGrid();
	}
	@Test
	public void test_changeValuesComputer_changes_the_values_if_no_errors_are_thrown(){
		gamefacade.changeValuescomputer();
		verify(game).PlaceShipsComputer();
	}
	@Test
	public void test_destroyChip_destroys_the_ship(){
		gamefacade.destroyShip();
		verify(game).destroyShip();
	}
	@Test
	public void test_getComputerGrid_gives_back_the_computer_grid(){
		gamefacade.getComputerGrid();
		verify(game).getComputergrid();
	}
	@Test
	public void test_getDestroyed_gives_back_the_destroyed_ship(){
		gamefacade.getDestroyed();
		verify(game).getDestroyed();
	}
	@Test
	public void test_(){
		gamefacade.getErrors();
		verify(game).getDestroyed();
	}
}
